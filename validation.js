const Joi = require("@hapi/joi");

// REGISTRATION VALIDATION
const registerValidation = data => {
  const schema = {
    name: Joi.string()
      .min(5)
      .required(),
    email: Joi.string().email({ minDomainSegments: 2 }),
    password: Joi.string()
      .min(6)
      .required()
  };

  return Joi.validate(data, schema);
};

const loginValidation = data => {
  const schema = {
    email: Joi.string().email({ minDomainSegments: 2 }),
    password: Joi.string()
      .min(6)
      .required()
  };

  return Joi.validate(data, schema);
};

module.exports.registerValidation = registerValidation;
module.exports.loginValidation = loginValidation;

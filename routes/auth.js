const express = require("express");
const verifyToken = require("../middlewares/verifyToken");
const {
  createUser,
  login,
  getAllUsers,
  deleteUser,
  editUser,
  getUserDetails
} = require("../controller/UserController");

const router = express.Router();

// REGISTER
router.post("/register", createUser);

// LOGIN
router.post("/login", login);

// PROTECTED ROUTES
// FETCH ALL
router.get("/all", verifyToken, getAllUsers);

// DELETE ONE
router.delete("/:id", verifyToken, deleteUser);

// EDIT
router.patch("/:id", verifyToken,editUser);

// GETONE
router.get("/:id", verifyToken, getUserDetails);

module.exports = router;

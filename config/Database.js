const Sequelize = require("sequelize");

const sequelize = new Sequelize("node-test", "root", "", {
  host: "localhost",
  dialect: "mysql"
});

module.exports = sequelize;

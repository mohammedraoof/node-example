const express = require("express");
const User = require("../models/User");
const sequelize = require("../config/Database");
const bcrypt = require("bcryptjs");
const { registerValidation, loginValidation } = require("../validation");
const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");

dotenv.config();

const createUser = async (req, res) => {
  const { error } = registerValidation(req.body);

  if (error) return res.send(error.details[0].message);

  const emailExists = await User.findOne({ email: req.body.email });

  if (emailExists) res.json({ message: "Email Already Exists" });

  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(req.body.password, salt);

  const newUser = {
    name: req.body.name,
    email: req.body.email,
    password: hashedPassword
  };
  sequelize
    .sync()
    .then(() => User.create(newUser))
    .then(user => {
      res.send(JSON.stringify(user));
    });
};

const login = async (req, res) => {
  const { error } = loginValidation(req.body);
  if (error) return res.send(error.details[0].message);

  let user = await User.findOne({ email: req.body.email });
  if (!user) return res.send("Email does not exist");
  user = user.dataValues;

  const validPass = await bcrypt.compare(req.body.password, user.password);
  if (!validPass) return res.send("Invalid Password");

  let token = jwt.sign({ id: user.id }, process.env.TOKEN_SECRET);

  res.header("auth-token", token).json({ token });
};

const getAllUsers = (req, res) => {
  User.findAll().then(data => {
    res.send(data);
  });
};

const deleteUser = async (req, res) => {
  const uid = req.params.id;
  user = await User.destroy({ where: { id: uid } });
  if (user) return res.json({ message: "User Deleted", user });
  res.json({ message: "No User Found" });
};

const editUser = async (req, res) => {
  const uid = req.params.id;
  try {
    user = await User.update({ name: req.body.name }, { where: { id: uid } });

    if (user)
      return res.json({ message: "User User Updated", updatedId: user });

    res.json({ message: "No User Found" });
  } catch (err) {
    res.json({ message: err });
  }
};

getUserDetails = async (req, res) => {
  const uid = req.params.id;
  try {
    user = await User.findOne({
      where: {
        id: uid
      }
    });
    if (user)
      return res.json({ id: user.id, name: user.name, email: user.email });
    res.json({ message: "No User Found" });
  } catch (err) {
    res.json({ message: err });
  }
};

module.exports = {
  createUser,
  login,
  getAllUsers,
  deleteUser,
  editUser,
  getUserDetails
};

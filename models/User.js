const Sequelize = require("sequelize");
const Model = Sequelize.Model;
const sequelize = require("../config/Database");

class User extends Model {}
User.init(
  {
    // attributes
    name: {
      type: Sequelize.STRING
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false

      // allowNull defaults to true
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false

      // allowNull defaults to true
    }
  },
  {
    sequelize,
    modelName: "user"
    // options
  }
);

module.exports = User;

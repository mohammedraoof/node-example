const express = require("express");
const dotenv = require("dotenv");
const app = express();


dotenv.config();

app.use(express.json()); 
app.use(express.urlencoded({ extended: true })); 
app.use("/api/user", require("./routes/auth"));

app.listen(3000, () => {
    console.log("Server Started ...");
});
